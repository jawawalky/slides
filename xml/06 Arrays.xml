<?xml version="1.0" encoding="utf-8"?>

<slides>

	<!-- ================================================================== -->
	<!-- =  Properties                                                    = -->
	<!-- ================================================================== -->

	<provider-name>Jukia Software</provider-name>
	<seminar-title>JavaScript Basics</seminar-title>
	<seminar-no>2200</seminar-no>
	<chapter-title>Arrays</chapter-title>
	

	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="chapter"/>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Basic Functionality
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>What is an Array?</heading>
			
			<list type="bullet">
			
				<item>
					An array is a multi-valued variable.
				</item>
				
				<item>
					In <name>JavaScript</name> arrays are objects.
				</item>
				
				<item>
					Arrays are dynamic, i.e. the size of an array is not fixed.
				</item>
				
				<item>
					<name>JavaScript</name> defines a class <code>Array</code>
					with various methods for array operations.
				</item>
				
				<item>
					The elements of an array are ordered and can be accessed
					by an index.
				</item>
				
				<item>
					The index is an integer and 0-based.
				</item>
				
				<item>
					The array knows the number of contained elements.
				</item>
				
			</list>
			
		</section>
			
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>How to Define an Array?</heading>
			
			<name>JavaScript</name> supports several ways of defining
			an array. The following array definitions are equivalent.
			
		</section>
		
		<section type="code"><![CDATA[var arr = new Array(1, 2, 3, 4, 5);]]></section>
		
		<section type="code"><![CDATA[var arr = Array(1, 2, 3, 4, 5);]]></section>
		
		<section type="code"><![CDATA[var arr = [1, 2, 3, 4, 5];]]></section>
		
		<section type="note">
		
			The arrays defined here, are defined on numbers, but of course
			any other data type is possible, too.
			 
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>How to Read Array Elements?</heading>
			
			<list type="bullet">
			
				<item>
					The elements of an array can be accessed with
					the <code>[]</code> operator.
				</item>
				
				<item>
					The length of an array is returned by the property
					<code>length</code>.
				</item>
				
			</list>
			
		</section>
		
		<section type="code"><![CDATA[let i;
for (i = 0; i < array.length; i++) { // <- Iterates over all
                                     //    array elements.
    console.log(array[i]);           // <- Retrieves the i-th
                                     //    array element.
}]]></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Defining an Empty Array</heading>
			
			We can define an empty array of a certain, initial length.
			The following array definitions are equivalent.
			
		</section>
		
		<section type="code"><![CDATA[var arr = new Array(5);]]></section>
		
		<section type="code"><![CDATA[var arr = Array(5);]]></section>
		
		<section type="code"><![CDATA[var arr = [];
arr.length = 5;]]></section>
		
		<section type="note">
		
			All defined arrays are empty, but have a length of 5.
			 
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>How to Populate an Array?</heading>
			
			<list type="bullet">
			
				<item>
					The elements of an array can be set with
					the <code>[]</code> operator.
				</item>
				
			</list>
			
		</section>
		
		<section type="code"><![CDATA[let array = [];
array[0] = 1;      // <- Index 0-based.
array[1] = 2;      // <- Array grows automatically.
array[3] = 4;      // <- Array needs not to be dense.]]></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The length Property</heading>
			
			The <code>length</code> property of an array object controls
			the current number of elements in an array.
			
		</section>
		
		<section type="code"><![CDATA[let array = [1, 2, 3];
array.length = 2;
console.log(array[0]);
console.log(array[1]);
console.log(array[2]);]]></section>
		
		<section>&gt; <code>1</code></section>
		<section>&gt; <code>2</code></section>
		<section>&gt; <code>undefined</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Array Methods
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Standard Array Methods</heading>
			
			<list type="bullet">
			
				<item>
					Arrays offer a wide range of methods, which make it
					easy and convenient to operate on them.
				</item>
				
				<item>
					Since arrays are dynamic in <name>JavaScript</name>,
					their methods offer a similar functionality like
					<code>Stream</code>s in <name>Java</name>.
				</item>
				
				<item>
					We can extract sub-arrays, check conditions on the array
					elements, filter elements, calculate aggregated data on
					them etc.
				</item>
				
			</list>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Standard Array Methods</heading>
			
			The following methods will be discussed
			
			<list type="bullet">
			
				<item><code>forEach(...)</code></item>
				<item><code>push(...)</code></item>
				<item><code>pop()</code></item>
				<item><code>concat(...)</code></item>
				<item><code>join(...)</code></item>
				<item><code>slice(...)</code></item>
				<item><code>sort(...)</code></item>
				<item><code>filter(...)</code></item>
				<item><code>map(...)</code></item>
				<item><code>every(...)</code></item>
				<item><code>some(...)</code></item>
				<item><code>reduce(...)</code></item>
				
			</list>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The forEach(...) Method</heading>
			
			The <code>forEach(...)</code> method takes a function argument
			often called a <name>consumer</name>. This function will be
			called during each iteration and says, what should be done with
			the current element.
			
		</section>
		
		<section type="code"><![CDATA[let array = [1, 2, 3];
array.forEach(e => console.log(e));]]></section>
		
		<section>&gt; <code>1</code></section>
		<section>&gt; <code>2</code></section>
		<section>&gt; <code>3</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The push(...) Method</heading>
			
			The <code>push(...)</code> method adds a new element to
			the end of the array.
			
		</section>
		
		<section type="code"><![CDATA[let array = [1, 2];
array.push(3);
array.forEach(e => console.log(e));]]></section>
		
		<section>&gt; <code>1</code></section>
		<section>&gt; <code>2</code></section>
		<section>&gt; <code>3</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The pop() Method</heading>
			
			The <code>pop()</code> method removes the last element
			of the array.
			
		</section>
		
		<section type="code"><![CDATA[let array = [1, 2, 3, 4];
let lastElement = array.pop();
array.forEach(e => console.log(e));
console.log(`Last Element: ${lastElement}`);]]></section>
		
		<section>&gt; <code>1</code></section>
		<section>&gt; <code>2</code></section>
		<section>&gt; <code>3</code></section>
		<section>&gt; <code>Last Element: 4</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The concat(...) Method</heading>
			
			The <code>concat(...)</code> method concatenates two arrays into
			one.
			
		</section>
		
		<section type="code"><![CDATA[let array = [1, 2];
array = array.concat([3, 4]);
array.forEach(e => console.log(e));]]></section>
		
		<section>&gt; <code>1</code></section>
		<section>&gt; <code>2</code></section>
		<section>&gt; <code>3</code></section>
		<section>&gt; <code>4</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The join(...) Method</heading>
			
			The <code>join(...)</code> method joins all array elements
			into one string. The elements are separated by some delimiter.
			The default delimiter is the comma sign (',').
			
		</section>
		
		<section type="code"><![CDATA[let array = [1, 2, 3];
let text = array.join(' - ');
console.log(text);]]></section>
		
		<section>&gt; <code>1 - 2 - 3</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The slice(...) Method</heading>
			
			The <code>slice(...)</code> method extracts a sub-array from
			the original array. The first index says, where the sub-array
			starts and the second, where it ends. The first index is
			inclusive and the second exclusive.
			
		</section>
		
		<section type="code"><![CDATA[array = [1, 2, 3, 4, 5];
console.log(array.slice(1, 4));]]></section>
		
		<section>&gt; <code>[ 2, 3, 4 ]</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The sort(...) Method</heading>
			
			The <code>sort(...)</code> method orders the elements of an array.
			We can specify a function, called a comparator, which defines
			the order of the elements. If no comparator is specified, then
			the natural order of the array elements is used, e.g. alphabetic
			order for strings.
			
		</section>
		
		<section type="code"><![CDATA[array = ['nine', 'one', 'three'];
console.log(array.sort((a, b) => a.length - b.length));]]></section>
		
		<section>&gt; <code>[ 'one', 'nine', 'three' ]</code></section>
		
		<section type="note">
		
			The strings are ordered by their length.
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The filter(...) Method</heading>
			
			The <code>filter(...)</code> method take a function, which defines
			a condition for array elements. Only the elements, which match that
			condition will stay in the array.
			
		</section>
		
		<section type="code"><![CDATA[array = [1, 2, 3, 4, 5];
console.log(array.filter(e => e % 2 == 0));]]></section>
		
		<section>&gt; <code>[ 2, 4 ]</code></section>
		
		<section type="note">
		
			Only even numbers match the filter.
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The map(...) Method</heading>
			
			The <code>map(...)</code> method allows us to map the array
			elements to some other elements.
			
		</section>
		
		<section type="code"><![CDATA[array = ['nine', 'one', 'three'];
console.log(array.map(e => e.length));]]></section>
		
		<section>&gt; <code>[ 4, 3, 5 ]</code></section>
		
		<section type="note">
		
			The result array contains numbers and not strings.
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The every(...) Method</heading>
			
			The <code>every(...)</code> method checks a condition on
			each array element. All array elements must match this condition,
			for this method to return <code>true</code>.
			
		</section>
		
		<section type="code"><![CDATA[array = [1, 3, 5];
console.log(array.every(e => e % 2 != 0));]]></section>
		
		<section>&gt; <code>true</code></section>
		
		<section type="note">
		
			Checks, if all array elements are odd numbers.
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The some(...) Method</heading>
			
			The <code>some(...)</code> method checks a condition on
			each array element. At least one array element must match
			this condition, for this method to return <code>true</code>.
			
		</section>
		
		<section type="code"><![CDATA[array = [2, 4, 6];
console.log(array.some(e => e % 2 != 0));]]></section>
		
		<section>&gt; <code>false</code></section>
		
		<section type="note">
		
			Checks, if at least one array element is an odd number.
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>The reduce(...) Method</heading>
			
			The <code>reduce(...)</code> method allows us to compose a single
			result from all array elements, e.g. if an array contains numbers,
			we can calculate the sum of all numbers.
			
		</section>
		
		<section type="code"><![CDATA[array = [2, 7, 4];
console.log(array.reduce((x, y) => x + y));]]></section>
		
		<section>&gt; <code>13</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
    	<section type="note">
    		Exercise: 0601_Arrays_Exercise
    	</section>

	</slide>


</slides>
