<?xml version="1.0" encoding="utf-8"?>

<slides>

	<!-- ================================================================== -->
	<!-- =  Properties                                                    = -->
	<!-- ================================================================== -->

	<provider-name>Jukia Software</provider-name>
	<seminar-title>JavaScript Basics</seminar-title>
	<seminar-no>2200</seminar-no>
	<chapter-title>Functions</chapter-title>
	

	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="chapter"/>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Overview
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>What is a Function?</heading>
			
			<list type="bullet">
			
				<item>
					A <name>function</name> is a piece of code that can be
					called from other locations in your code.
				</item>
				
				<item>
					Data can be passed to a function as parameters.
				</item>
				
				<item>
					A result can be returned from a function as a return value.
				</item>
				
				<item>
					A function can have a name, but <name>JavaScript</name>
					also allows anonymous functions.
				</item>
				
				<item>
					A function can be assigned to a variable.
				</item>
				
				<item>
					A function is invoked by the <code>()</code> operator.
				</item>
				
				<item>
					A function is defined by the keyword <code>function</code>.
				</item>
				
				<item>
					A new type of functions has been introduced by
					the so-called <name>arrow functions</name>.
				</item>
				
			</list>
			
		</section>
			
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Named Functions
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Defining a Function</heading>
			
		</section>
		
		<section type="code"><![CDATA[function sayHello() {
  console.log('Hello');
}]]></section>

		<section>
		
			<list type="bullet">
			
				<item>
					The keyword <code>function</code> starts the function
					definition.
				</item>
				
				<item>
					It is followed by the function's name.
				</item>
				
				<item>
					Every function must have round brackets. Here the function
					parameters are defined, if the function accepts input.
				</item>
				
				<item>
					The curly brackets delimit the body of the functions, i.e.
					the code executed by the function.
				</item>
				
				<item>
					The result of a function can be returned by the keyword
					<code>return</code>.
				</item>
				
			</list>
		
		</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Calling a Function</heading>
			
			A function can be called by its name and with the required
			input arguments.
			
		</section>
		
		<section type="code"><![CDATA[function sayHello() {
  console.log('Hello');
}

sayHello();]]></section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Examples</heading>
			
		</section>
		
		<section type="code"><![CDATA[function printSum(a, b) {
  console.log(`${a} + ${b} = ${a + b}`);
}

function add(a, b) { return a + b; }
function mlt(a, b) { return a * b; }
function sqr(x) { return mlt(x, x); }

function squareSum(a, b) {
  return add(sqr(a), sqr(b));
}

let r = squareSum(3, 4);]]></section>

	<section>
	
		&gt; <code>r = 25</code>
		
	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Assigning a Function to a Variable</heading>
			
			<list type="bullet">
			
				<item>
					A function can be assigned to a variable.
				</item>
				
				<item>
					The function can be invoked through this variable and
					with the <code>()</code> operator.
				</item>
				
			</list>
			
		</section>
		
		<section type="code"><![CDATA[function add(x, y) { return x + y; }

let f = add;
let r = f(3, 4);]]></section>

	<section>
	
		&gt; <code>r = 7</code>
		
	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Recursive Function Calls</heading>
			
			As we saw, a function can call another function. In such way
			a function can also call itself. That is called a recursive
			function call.
			
		</section>
		
		<section type="code"><![CDATA[function factorial(n) {
  switch (n) {
  	case 0:
  	case 1:
  	  return 1;
  	default:
  	  return n * factorial(n - 1);
  }
}

let r = factorial(4);]]></section>

	<section>
	
		&gt; <code>r = 24</code>
		
	</section>
	
	<section type="note">
	
		Be careful with recursive function calls that you don't get an endless
		loop!
		
	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
    	<section type="note">
    		Exercise: 0401_Functions_Exercise
    	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Anonymous Functions
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>A Function without a Name</heading>
			
			<list type="bullet">
			
				<item>
					<name>JavaScript</name> allows us to define functions
					without a name.
				</item>
				
				<item>
					Since we cannot reference them, because they do not have
					a name, they need to be assigned to some variable.
				</item>
				
			</list>
			
		</section>
		
		<section type="code"><![CDATA[let add = function(x, y) { return x + y; }
let r = add(3, 4);]]></section>

	<section>
	
		&gt; <code>r = 7</code>
		
	</section>
	
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Direct Invocation</heading>
			
			We can define a function and invoke it directly by applying
			the <code>()</code> operator.
			
		</section>
		
		<section type="code"><![CDATA[function() {
  console.log('Hello');
}();]]></section>

	<section>
	
		We just put the <code>()</code> operator behind the function body.
		
	</section>
	
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Function as Parameter</heading>
			
			Anonymous functions are often used, when a function has to be
			passed as a parameter to another function.
			
		</section>
		
		<section type="code"><![CDATA[function calculate(operation) {
  const values = [1, 2, 3, 4, 5];
  for (value of values) {
    console.log(operation(value));
  }
}

let square = function(x) { return x * x; }
let cube   = function(x) { return x * x * x; }

calculate(square);
calculate(cube);]]></section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
    	<section type="note">
    		Exercise: 0402_Anonymous_Functions_Exercise
    	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Function Scope
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>What is Scope?</heading>
			
			<list type="bullet">
			
				<item>
					All elements in the same scope can see each other.
				</item>
				
				<item>
					All elements of the outer scope are visible to those of
					the inner scope.
				</item>
				
				<item>
					A function defines its own scope, so elements defined
					inside a function cannot be accessed from outside
					the function.
				</item>
				
				<item>
					A variable defined in the inner scope with the same name
					as a variable defined in the outer scope, shadows the outer
					variable.
				</item>
				
			</list>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Scope Example</heading>
			
		</section>
			
		<section type="code"><![CDATA[var a = 1; var b = 2; var c = 3;

function global() { return `${a} ${b} ${c}`; }
console.log(`A) ${global()}`);

function parent() {
    var b = 4;
    function child() {
        var c = 5;
        return `${a} ${b} ${c}`;
    }
    return child();
}
console.log(`B) ${parent()}`);]]></section>

	<section>
	
		&gt; <code>A) 1 2 3</code>
	
	</section>

	<section>
	
		&gt; <code>B) 1 4 5</code>
	
	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Recursive Function Calls</heading>
			
			We have seen recursive function calls before. There are three ways
			to call a function inside its body.
			
			<list type="bullet">
			
				<item>by its name</item>
				<item><code>arguments.callee</code></item>
				<item>
					by an <name>in-scope</name> variable pointing to
					the function
				</item>
				
			</list>
			
			The following alternatives are fully equivalent.
			
		</section>
			
		<section type="code"><![CDATA[let fac = function factorial(n) {
    return (n < 1) ? 1 : n * factorial(n - 1);
// Alternative 1:
//    return (n < 1) ? 1 : n * fac(n - 1);
// Alternative 2:
//    return (n < 1) ? 1 : n * arguments.callee(n - 1);
}]]></section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Closures
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>What is a Closure?</heading>
			
			<list type="bullet">
			
				<item>
					Inner functions have access to variables in the outer
					function.
				</item>
				
				<item>
					The outer function can return a reference to the inner
					function.
				</item>
				
				<item>
					Although the outer function has terminated, the inner
					function keeps the variables of it alive and allows access
					to them.
				</item>
				
				<item>
					Since function variables are not accessible outside
					the functions the construction is called
					a <name>closure</name>.
				</item>
				
			</list>
			
		</section>
			
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Example</heading>
			
		</section>
			
		<section type="code"><![CDATA[var pet = function(name) {
    var getName = function() {
        return name;
    }
    return getName;
}
var myPet = pet('Tiger');
console.log(myPet());]]></section>

		<section>
		
			&gt; <code>Tiger</code>
			
		</section>
		
		<section type="note">
		
			While the outer function <code>pet(...)</code> has terminated,
			the inner function <code>getName()</code> can still access
			the variable <code>name</code> defined in the outer function.
			
		</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Building 'Objects' using Functions</heading>
			
			<list type="bullet">
			
				<item>
					Inner functions and closures can be used to build
					object-like structures with data hiding.
				</item>
			
				<item>
					We can use validation, when setting the internal data.
				</item>
			
			</list>
			
		</section>
			
		<section type="note">
		
			We will learn about that in the chapter <name>Objects</name>.
			
		</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
    	<section type="note">
    		Exercise: 0403_Function_Scope_Exercise
    	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Function Parameters
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Function Arguments</heading>
			
			<list type="bullet">
			
				<item>
					As we have seen before, there exists an internal object
					<code>arguments</code>. It contains the values passed
					to the function.
				</item>
			
				<item>
					It is provided by <name>JavaScript</name> in each function.
				</item>
				
				<item>
					It contains
					
					<list type="bullet">
					
						<item>
							a reference to itself: <code>arguments.callee()</code>,
						</item>
						
						<item>
							the number of arguments: <code>arguments.length</code> and
						</item>
						
						<item>
							references to each argument: <code>arguments[i]</code>
						</item>
						
					</list>
				</item>
			
			</list>
			
		</section>
			
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Example</heading>
			
		</section>
			
		<section type="code"><![CDATA[function doConcat(separator) {
   var result = ''; var i;
   for (i = 1; i < arguments.length; i++) {
      result += arguments[i] + separator;
   }
   return result;
}
doConcat(';', 'one', 'two', 'three');
doConcat('.', 'person', 'name');]]></section>

		<section>
		
			&gt; <code>one;two;three;</code>
			
		</section>
		
		<section>
		
			&gt; <code>person.name.</code>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Default Arguments</heading>
			
			Since <name>ECMAScript 2015</name> you can use default arguments.
			They apply, when a certain function parameter has not been set.
			
		</section>
			
		<section type="code"><![CDATA[function add(a, b = 0) {
  return a + b;
}

console.log(add(5));
console.log(add(5, 3));]]></section>

		<section>&gt; <code>5</code></section>
		<section>&gt; <code>8</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Without Default Arguments</heading>
			
			Without default arguments the code would be a bit more complicated
			
		</section>
			
		<section type="code"><![CDATA[function add(a, b) {
  b = typeof b !== 'undefined' ?  b : 0;
  return a + b;
}

console.log(add(5));
console.log(add(5, 3));]]></section>

		<section>&gt; <code>5</code></section>
		<section>&gt; <code>8</code></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Rest Parameters</heading>
			
			As we saw, we can access an unlimited number or arguments with
			the <code>arguments</code> object. That is somehow magic to
			the caller of a function, since the signature of the function does
			not reveal that feature. By <name>rest parameters</name> we can
			show that possibility clearly in the function signature.
			
		</section>
			
		<section type="code"><![CDATA[function hello(greeting, ...folks) {
  for (person of folks) {
  	console.log(greeting + ' ' + person);
  }
}
hello('Hi', 'Toby', 'Sandy', 'Bob');]]></section>

		<section>
		
			&gt; <code>Hi Toby</code>
			
		</section>
		
		<section>
		
			&gt; <code>Hi Sandy</code>
			
		</section>
		
		<section>
		
			&gt; <code>Hi Bob</code>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
    	<section type="note">
    		Exercise: 0404_Function_Parameters_Exercise
    	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Arrow Functions
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>What is an Arrow Function?</heading>
			
			<list type="bullet">
			
				<item>
					<name>Arrow functions</name> introduce a very concise way
					to define and express a function.
				</item>
			
				<item>
					<name>Arrow functions</name> are always anonymous.
				</item>
				
				<item>
					An <name>arrow function</name> has the form
					<code>(&lt;arguments&gt;) =&gt; { &lt;code&gt; }</code>
				</item>
				
				<item>
					They are very often used, when a function is needed as
					a parameter argument.
				</item>
				
			</list>
			
		</section>
			
		<section>
		
			<heading>Example</heading>
			
		</section>
		
		<section type="code"><![CDATA[let hello = (name) => { return 'Hello ' + name; }]]></section>
		
		<section>
		
			If the parameter list only contains one parameter then we can
			skip the round brackets.
			
		</section>
		
		<section type="code"><![CDATA[let hello = name => { return 'Hello ' + name; }]]></section>
		
		<section>
		
			If the function code only contains one statement, then we can drop
			the curly brackets and the <code>return</code> statement.
			
		</section>
		
		<section type="code"><![CDATA[let hello = name => 'Hello ' + name;]]></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Anonymous Function vs Arrow Function</heading>
			
			We can define an add operation as an anonymous function
			
		</section>
			
		<section type="code"><![CDATA[let add = function(x, y) { return x + y; }]]></section>
		
		<section>
		
			or as an arrow function
			
		</section>
		
		<section type="code"><![CDATA[let add = (x, y) => x + y;]]></section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Arrow Function without Parameters</heading>
			
			An arrow function, which does not take any arguments has
			an empty parameter list. The parameter list is denote by round
			brackets.
			
		</section>
			
		<section type="code"><![CDATA[let hello = () => console.log('Hello');
hello();]]></section>

		<section>
		
			&gt; <code>Hello</code>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Defining and Invoking Arrow Function</heading>
			
			We can define and invoke an arrow function in one statement.
			
		</section>
			
		<section type="code"><![CDATA[((x, y) => console.log(`${x} + ${y} = ${x + y}`))(3, 4);]]></section>

		<section>
		
			&gt; <code>3 + 4 = 7</code>
			
		</section>
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section type="note">
		
			In the next chapter we will see, how functions can be used
			in objects. Here the keyword <code>this</code> will be introduced.
			<code>this</code> behaves differently in normal functions and
			arrow functions!
			
		</section>
			
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
    	<section type="note">
    		Exercise: 0405_Arrow_Functions_Exercise
    	</section>

	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide type="section">
		
		Predefined Functions
		
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Top-level, Built-in Functions</heading>
		
			<table>
			
				<column width="35%"/>
				<column width="65%"/>
				
				<row>
					<cell type="header">Function</cell>
					<cell type="header">Purpose</cell>
				</row>
				
				<row>
					<cell>
						<code>eval()</code>
					</cell>
					<cell>
						Evaluates the <name>JavaScript</name> code in some
						string.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>uneval()</code>
					</cell>
					<cell>
						Creates a string with the source code of
						an <code>Object</code>.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>isFinite()</code>
					</cell>
					<cell>
						Checks, if a value represents a finite number.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>isNaN()</code>
					</cell>
					<cell>
						Checks, if a value is <name>not-a-number</name>.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>parseFloat()</code>
					</cell>
					<cell>
						Converts a string to a floating point number.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>parseInt()</code>
					</cell>
					<cell>
						Converts a string to an integer number.
					</cell>
				</row>
				
			</table>
		
		</section>
			
	</slide>


	<!-- ================================================================== -->
	<!-- =  Slide                                                         = -->
	<!-- ================================================================== -->

	<slide>
	
		<section>
		
			<heading>Top-level, Built-in Functions</heading>
		
			<table>
			
				<column width="35%"/>
				<column width="65%"/>
				
				<row>
					<cell type="header">Function</cell>
					<cell type="header">Purpose</cell>
				</row>
				
				<row>
					<cell>
						<code>decodeURI()</code>
					</cell>
					<cell>
						Decodes a URI encoded by <code>encodeURI()</code>.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>decodeURIComponent()</code>
					</cell>
					<cell>
						Decodes a URI component encoded by
						<code>encodeURIComponent()</code>.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>encodeURI()</code>
					</cell>
					<cell>
						Encodes a URI by replacing certain characters
						by their escape sequence.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>encodeURIComponent()</code>
					</cell>
					<cell>
						Encodes a URI component by replacing certain characters
						by their escape sequence.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>escape()</code>
					</cell>
					<cell>
						<strong>Deprecated!</strong>
						Use <code>encodeURI()</code> or
						<code>encodeURIComponent()</code> instead.
					</cell>
				</row>
				
				<row>
					<cell>
						<code>unescape()</code>
					</cell>
					<cell>
						<strong>Deprecated!</strong>
						Use <code>decodeURI()</code> or
						<code>decodeURIComponent()</code> instead.
					</cell>
				</row>
				
			</table>
		
		</section>
			
	</slide>


</slides>
